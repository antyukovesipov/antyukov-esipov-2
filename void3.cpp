#include <iostream>
#include <windows.h> 
#include <conio.h>
#include <cmath>
using namespace std;

char str[] = "The 1000 days. What the problem? Imagine you should be involved in several activities. You are young, active, eager to study, you have to do sports, work, stroll and chat with friends. Of course young people need time for it. As a rule the whole week is planned in a way that there is no time for the rest.";

void TransferNumb()
{
	int i = 0, t = 0, numb = 0, size = 0, length = strlen(str) + 1;
	char numbChar[10], setChar[12] = {'0','1','2','3','4','5','6','7','8','9','A','B'};
	int setInt[12];
	while (t != length)
	{
		cout << str[t];
		if ((str[t] == '.') || (str[t] == '?') || (str[t] == '!'))
		{
			cout << endl;
			t++;
		}
		t++;
	}
	cout << endl;
	while ((str[i] != '.') && (str[i] != '?') && (str[i] != '!'))
	{
		if (str[i] >= '0' && str[i] <= '9')
		{
			while (str[i] != ' ')
			{
				setInt[size] = str[i] - '0';
				size++;
				i++;
			}
			break;
		}
		i++;
	}
	size--;
	int j = 0;
	while (size >= 0)
	{
		numb += setInt[size] * pow(10, j);
		size--;
		j++;
	}
	cout << "The original number in 10 system: " << numb << endl;
	int k = 0;
	cout << "The original number in 12 system: ";
	while (numb > 0)
	{
		int temp = numb % 12;
		numbChar[k] = setChar[temp];
		numb /= 12;
		k++;
	}
	k--;
	while (k >= 0)
	{
		cout << numbChar[k];
		k--;
	}
	cout << endl;
	system("pause");
}

int main() {}