#include <iostream>
#include <windows.h> 
#include <conio.h>
using namespace std;

char str[] = "What the problem? Imagine you should be involved in several activities. You are young, active, eager to study, you have to do sports, work, stroll and chat with friends. Of course young people need time for it. As a rule the whole week is planned in a way that there is no time for the rest.";

void ShowText()
{
	HANDLE hOUTPUT = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hOUTPUT, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
	int i = 0, count = 0, length = strlen(str) + 1;
	while (i != length)
	{
		cout << str[i];
		if ((str[i] == '.') || (str[i] == '?') || (str[i] == '!'))
		{
			cout << endl;
			i++;
		}
		i++;
	}
	cout << endl;
	system("pause");
	system("cls");
	i = 0;
	while (i != length)
	{
		if ((str[i] == 'A') || (str[i] == 'E') || (str[i] == 'I') || (str[i] == 'O') || (str[i] == 'U') || (str[i] == 'Y'))
		{
			count++;
			SetConsoleTextAttribute(hOUTPUT, FOREGROUND_GREEN | FOREGROUND_RED);
		}
		cout << str[i];
		if ((str[i] == '.') || (str[i] == '?') || (str[i] == '!'))
		{
			SetConsoleTextAttribute(hOUTPUT, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			cout << endl;
			i++;
		}
		i++;
	}
	SetConsoleTextAttribute(hOUTPUT, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
	cout << endl;
	cout << "Тumber of sentences starting with a vowel: " << count << endl << endl;
	system("pause");
}

int main() {}